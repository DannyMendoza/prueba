﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DemoPruebaFinal
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("        DATOS GENERALES DE EMPLEADOS Y ESTUDIANTES       ");
            Console.WriteLine();

            Console.WriteLine(" Datos Docentes de La Facultad ");
            Console.WriteLine();

            Profesores profesor1 = new Profesores();
            profesor1.Nombres = "Juan Manuel";
            profesor1.Apellidos = " Vega Macias";
            profesor1.NumeroIdentificacion = 1302838399;
            profesor1.EstadoCivil = "Soltero";

            Profesores profesor2 = new Profesores();
            profesor2.Nombres = "Daniel Jose";
            profesor2.Apellidos = " Macias Mera";
            profesor2.NumeroIdentificacion = 1302829869;
            profesor2.EstadoCivil = "Casado";

            Profesores profesor3 = new Profesores();
            profesor3.Nombres = "Juana Maria";
            profesor3.Apellidos = " Delgado Molina";
            profesor3.NumeroIdentificacion = 1203847281;
            profesor3.EstadoCivil = "Viuda";
            profesor3.SalarioFij = 1000;
            profesor3.HorasExtras = 25;

            profesor1.ListaProfesores.Add(profesor1);
            profesor2.ListaProfesores.Add(profesor2);
            profesor3.ListaProfesores.Add(profesor3);


            foreach (var profesores in profesor1.ListaProfesores)
            {
                Console.WriteLine("Nombres:" + " " + profesores.Nombres + " " + "/" + "Apellidos:" + " " + profesores.Apellidos + " " + "/" + "N.Identifiacion:" + " " + profesores.NumeroIdentificacion + " " + "/" + "Estado Civil:" + " " + profesores.EstadoCivil);
            }
            Console.WriteLine();
            foreach (var profesores in profesor2.ListaProfesores)
            {
                Console.WriteLine("Nombres:" + " " + profesores.Nombres + " " + "/" + "Apellidos:" + " " + profesores.Apellidos + " " + "/" + "N.Identifiacion:" + " " + profesores.NumeroIdentificacion + " " + "/" + "Estado Civil:" + " " + profesores.EstadoCivil);
            }
            Console.WriteLine();
            foreach (var profesores in profesor3.ListaProfesores)
            {
                Console.WriteLine("Nombres:" + " " + profesores.Nombres + " " + "/" + "Apellidos:" + " " + profesores.Apellidos + " " + "/" + "N.Identifiacion:" + " " + profesores.NumeroIdentificacion + " " + "/" + "Estado Civil:" + " " + profesores.EstadoCivil);
            }
            Console.WriteLine();


            profesor1.ListaProfesoresProgr.Add(profesor1);
            profesor2.ListaProfesoresRedes.Add(profesor2);
            profesor3.ListaProfesoresRequisitos.Add(profesor3);

            Console.WriteLine("Datos Estudiantes de la Facultad");
            Console.WriteLine();

            Estudiantes estudiantes1 = new Estudiantes();
            estudiantes1.Nombres = " Luis Armando";
            estudiantes1.Apellidos = "Menendez Mendoza";
            estudiantes1.NumeroIdentificacion = 1293837182;
            estudiantes1.EstadoCivil = " Soltero";


            Estudiantes estudiantes2 = new Estudiantes();
            estudiantes2.Nombres = " Karla Valeria";
            estudiantes2.Apellidos = " Mendoza Macias";
            estudiantes2.NumeroIdentificacion = 1209227182;
            estudiantes2.EstadoCivil = " Soltera";

            estudiantes1.ListaEstudiantes.Add(estudiantes1);
            estudiantes2.ListaEstudiantes.Add(estudiantes2);


            foreach (var estudiantes in estudiantes1.ListaEstudiantes)
            {
                Console.WriteLine("Nombres:" + " " + estudiantes.Nombres + " " + "/" + "Apellidos:" + " " + estudiantes.Apellidos + " " + "/" + "N.Identifiacion:" + " " + estudiantes.NumeroIdentificacion + " " + "/" + "Estado Civil:" + " " + estudiantes.EstadoCivil);
            }
            Console.WriteLine();
            foreach (var estudiantes in estudiantes2.ListaEstudiantes)
            {
                Console.WriteLine("Nombres:" + " " + estudiantes.Nombres + " " + "/" + "Apellidos:" + " " + estudiantes.Apellidos + " " + "/" + "N.Identifiacion:" + " " + estudiantes.NumeroIdentificacion + " " + "/" + "Estado Civil:" + " " + estudiantes.EstadoCivil);
            }
            Console.WriteLine();
            
            Console.WriteLine(" Datos personal de Servicio");
            Console.WriteLine();

            Servicio servicio1 = new Servicio();
            servicio1.Nombres = "Ramon Aquiles";
            servicio1.Apellidos = " Ponce Zambrano";
            servicio1.NumeroIdentificacion = 1323819120;
            servicio1.EstadoCivil = "Casado";

            
            Servicio servicio2 = new Servicio();
            servicio2.Nombres = "Jose Gregorio";
            servicio2.Apellidos = " Aguirre Cedeño";
            servicio2.NumeroIdentificacion = 1323819120;
            servicio2.EstadoCivil = "Viudo";

            servicio1.ListaServicio.Add(servicio1);
            servicio2.ListaServicio.Add(servicio2);

            foreach (var servicio in servicio1.ListaServicio)
            {
                Console.WriteLine("Nombres:" + " " + servicio.Nombres + " " + "/" + "Apellidos:" + " " + servicio.Apellidos + " " + "/" + "N.Identifiacion:" + " " + servicio.NumeroIdentificacion + " " + "/" + "Estado Civil:" + " " + servicio.EstadoCivil);
            }
            Console.WriteLine();
            foreach (var servicio in servicio2.ListaServicio)
            {
                Console.WriteLine("Nombres:" + " " + servicio.Nombres + " " + "/" + "Apellidos:" + " " + servicio.Apellidos + " " + "/" + "N.Identifiacion:" + " " + servicio.NumeroIdentificacion + " " + "/" + "Estado Civil:" + " " + servicio.EstadoCivil);
            }
            Console.WriteLine();

            Console.WriteLine("Datos departamentos de los docentes");
            Console.WriteLine();

            foreach (var departamentos in profesor1.ListaProfesoresProgr)
            {
                Console.WriteLine( departamentos.Nombres + " " + departamentos.Apellidos + " " + "pertenece al departamento de Programacion");
            }
            Console.WriteLine();
            foreach (var departamentos in profesor2.ListaProfesoresRedes)
            {
                Console.WriteLine( departamentos.Nombres + " " + departamentos.Apellidos + " " + "pertenece al departamento de Redes");
            }
            Console.WriteLine();
            foreach (var departamentos in profesor3.ListaProfesoresRequisitos)
            {
                Console.WriteLine( departamentos.Nombres + " " + departamentos.Apellidos + " " + "pertenece al departamento de Requisitos");
            }
            Console.WriteLine();
            Console.WriteLine("Año de incorporacion de Profesores");
           

            
          Double MdatosP = profesor1.Salarioprofesor1 ();
              
            Double MdatosP2 = profesor2.Salarioprofesor2();


            Console.WriteLine("El sueldo del profesor" + " " + profesor1.Nombres  + " " + "es" + " " + MdatosP);
            


            Double CalculHxtra = profesor3.Horastrabajadas();

            if (CalculHxtra > 8)
            {
                int totalpago = profesor3.SalarioFij + profesor3.HorasExtras;

                Console.WriteLine("Si las horas trabajadas son mayores a 8, su nuevo sueldo es" + " " + totalpago);


            }

            else
            {
                Console.WriteLine("Su sueldo fijo es" + " " + profesor3.SalarioFij);
            }

            Console.ReadLine();







        }
    }
}
